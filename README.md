# Movies and Series Automatic Foldering System (MASAFS)


This applications allows you to organize your movies and TV shows automatically without needing to going to find different episodes or all the different versions of the movie. This project goes through all of the directories and sub-directories of a selected directory recursively, and organize all of the files accordingly. Also, **MASAFS** will looks for **_Persian_** and **_Arabic_** subtitles that are not encoded correctly, and will change their encode from '_Windows-1256_' to '_UTF-8_', so that they can be shown correctly while playing.

Moreover, This project was developed using Java programming language and regular expressions (RegEx) to find the patterns in different movies and TV shows naming sterotypes.

## Installation
### Compile it yourself
This project was developed by Apache Netbeans 12.3, therefore to install this project you need to clone the entire project and open the project in Netbeans and build & run it.

to clone the project use the command below:
```bash
git clone https://gitlab.com/parham-box/movies-and-series-automatic-foldering-system.git
```


### Pre-Compiled version
Download the released .jar file. That's it!


## Usage
### DO NOT SELECT AN IMPORTANT DIRECTORY FOR THIS APP TO SORT. 
After running the project a window will appear.
![Main window of the app](sc1.png)
Click on **select**, and select the directory that needs to be organized.
![Main window of the app with selected path](sc2.png)

click **sort** after selecting the path  
### be sure to have a backup of the selected folder before sorting
 
## Supported formats
This app currently organizes files with these extensions:
 **mp4**, **avi**, **MOV**, **jpg**, **jpeg**, **png**, **gif**, **mp3**, **m4a**, **srt**, **sub**, **idx**,**mkv**


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License
[MIT](https://choosealicense.com/licenses/mit/)
